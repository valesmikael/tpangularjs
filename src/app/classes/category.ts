import { Product, ProductLiteral } from './product';


export class Category {

    public id: number;
    public label: string;
    public products: Product[] = [];
  

    constructor( categoryLiteral: CategoryLiteral ) {
        this.id = categoryLiteral.id;
        this.label = categoryLiteral.label;

        if ( categoryLiteral.products ){
            for ( const product_json of categoryLiteral.products ){
                this.products.push(
                    new Product( product_json)
                );
            }
        }
    }
}

export interface CategoryLiteral {
    
    id: number;
    label: string;
    products?: ProductLiteral[];
}