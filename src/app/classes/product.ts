export class Product {

    public id: number;
    public titre: string;
    public marque: string;
    public descriptif: string;
    public prix: number;
    public reference: string;

    constructor( productLiteral: ProductLiteral ){
        this.id = productLiteral.id;
        this.titre = productLiteral.titre;
        this.marque = productLiteral.marque;
        this.descriptif = productLiteral.descriptif;
        this.prix = productLiteral.prix;
        this.reference = productLiteral.reference;
    }

}

export interface ProductLiteral {
    
     id: number;
     titre: string;
     marque: string;
     descriptif: string;
     prix: number;
     reference: string;

}