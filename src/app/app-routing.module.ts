import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoriesDetailsComponent } from './components/categoriesdetails/categories.details.component';
import { HomeComponent } from './components/home/home.component';
import { ProductDetailsComponent } from './components/productdetails/product.details.component';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'categoriesdetails/:id', component: CategoriesDetailsComponent },
  { path: 'productdetails/:id', component: ProductDetailsComponent },
  

];

@NgModule({
  
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
