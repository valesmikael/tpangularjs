import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/classes/category';
import { CategoriesService } from 'src/app/services/categories/categories.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  public categories: Category[] = [];

  constructor( private categoriesService: CategoriesService ) {}

    ngOnInit() {

      this
          .categoriesService
          .getCategories()
          .then( (json) => {
            for ( const category_json of json) {
              const category: Category = new Category(category_json);
              this.categories.push( category );              
            }
          });

    }

}
