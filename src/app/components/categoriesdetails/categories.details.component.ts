import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { CategoriesService } from 'src/app/services/categories/categories.service';
import { Category } from 'src/app/classes/category';
import { Product } from 'src/app/classes/product';


@Component({
  selector: 'app-categoriesdetails',
  templateUrl: './categories.details.component.html',
  styleUrls: ['./categories.details.component.scss']
})
export class CategoriesDetailsComponent implements OnInit {
  

  public category: Category;
  public product: Product;
  
    
  constructor( private route: ActivatedRoute,
               private categoriesService: CategoriesService ) { }

  ngOnInit(){
// permet de rebouter l'élément qui change dans l'url ici c'est l'id
    this.route.params.subscribe(( params: ParamMap ) => {

      const id: number = parseInt( params['id'] );
      //---------------------------
      this
      .categoriesService
      .getCategory( id )
        .then( (json) => {
        this.category = new Category( json );
        console.log( this.category );
      } );

    });

   

  }



}
