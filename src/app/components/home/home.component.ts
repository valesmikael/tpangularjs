import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/services/products/products.service';
import { Product } from 'src/app/classes/product';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public products: Product[] = [];

  constructor( private productsService: ProductsService ) { }

  ngOnInit() {
    this
    .productsService
    .getProducts()
            .then( (json) => {
      for ( const product_json of json ) {
        const product: Product = new Product(product_json);
        this.products.push( product );
      }
    }); 
  }

}
