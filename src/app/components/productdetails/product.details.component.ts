import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/classes/product';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ProductsService } from 'src/app/services/products/products.service';


@Component({
  selector: 'app-productdetails',
  templateUrl: './product.details.component.html',
  styleUrls: ['./product.details.component.scss']
})
export class ProductDetailsComponent implements OnInit {

  public product: Product;

  constructor( private route: ActivatedRoute,
    private productsService: ProductsService ) { }

  ngOnInit(){
  // permet de rebouter l'élément qui change dans l'url ici c'est l'id
      this.route.params.subscribe(( params: ParamMap ) => {

      const id: number = parseInt( params['id'] );
      //---------------------------
      this
      .productsService
      .getProduct( id )
      .then( (json) => {
      this.product = new Product( json );
      console.log( this.product );
      } );

    });



  }



}
