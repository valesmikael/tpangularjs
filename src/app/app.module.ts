import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';

import { MainComponent } from './components/main/main.component';
import { NavComponent } from './components/nav/nav.component';
import { HomeComponent } from './components/home/home.component';
import { ProductComponent } from './components/product/product.component';
import { CategoriesDetailsComponent } from './components/categoriesdetails/categories.details.component';
import { ProductDetailsComponent } from './components/productdetails/product.details.component';

import { CategoriesService } from './services/categories/categories.service';
import { ProductsService } from './services/products/products.service';


@NgModule({
  declarations: [
    MainComponent,
    NavComponent,
    HomeComponent,     
    ProductComponent,
    CategoriesDetailsComponent,
    ProductDetailsComponent,
  ],
  
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [CategoriesService, ProductsService],
  bootstrap: [MainComponent]
})
export class AppModule { }
