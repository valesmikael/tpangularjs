import { Injectable } from '@angular/core';
import { ProductLiteral } from 'src/app/classes/product';


@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  

  readonly base_url: string ='http://192.168.42.182:8000/api/products';

  private json_products: ProductLiteral[] = [];

  getProducts(): Promise<ProductLiteral[]> {

    if ( !this.json_products.length ) {
      const response_fetched: Promise<Response> =fetch(`${this.base_url}`);
      const json_fetched: Promise<ProductLiteral[]> = response_fetched.then( response => response.json() );

        json_fetched.then( (json) => {
          this.json_products = json;
        });

        return json_fetched;
    }else return Promise.resolve( this.json_products);

  }

  getProduct( id: number ): Promise<ProductLiteral> {

    return fetch( this.base_url + '/' + id).then( response => response.json( ));
  }


  
}
