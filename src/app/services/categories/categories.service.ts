import { Injectable } from '@angular/core';
import { CategoryLiteral } from 'src/app/classes/category';


@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  readonly base_url: string = 'http://192.168.42.182:8000/api/categories';

  private json_categories: CategoryLiteral[] = [];

  getCategories(): Promise<CategoryLiteral[]> {

    if ( !this.json_categories.length ) {
      const response_fetched: Promise<Response> = fetch(`${this.base_url}`);
      const json_fetched: Promise<CategoryLiteral[]> = response_fetched.then( response => response.json() );

        json_fetched.then( (json) => {
          this.json_categories = json;
        });

        return json_fetched;
    }else Promise.resolve( this.json_categories);


  }

  getCategory( id: number ): Promise<CategoryLiteral> {

    return fetch( this.base_url + '/' + id).then( response => response.json( ));
  }

}


